﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    public float minAttackPower;
    public float maxAttackPower;

    public GameObject gameHUD;

    public float cheerPower;
    public int points = 0;

    [HideInInspector]
    public bool isInDash = false;
    public float dashCoolDown;

    public float movementSpeed = 5f;
    public float jumpForce = 100f;
    private bool canMove = true;
    private bool doubleJumped = false;

    public bool isChargingKick = false;
    private bool isKicking = false;
    public float chargeTime = 2f;
    private float currentChargeTime = 0f;

    public bool canChangeKickDirection = false;
    private Vector2 kickDirection;
    public GameObject kickArrow;

    private float rotationSpeedKickDirection = 50f;
    [SerializeField]
    private float currentKickDirection;

    public BoxCollider2D kickBoxCollider;
    private KickHitBoxBehaviour kickBoxBehaviour;
    private Rigidbody2D rb;

    public InputList input;

    private bool isFacingRight = true;

    private float currentScaleX;

    public bool isGrounded;

    public bool isInvencible = false;

    public Animator charAnim;

    public Player player;


    public float fallMultiplier = 4f;

    private bool shouldMoveX;

    //public Skill skill;

    private void Start()
    {
        charAnim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody2D>();
        kickBoxBehaviour = kickBoxCollider.gameObject.GetComponent<KickHitBoxBehaviour>();
        currentScaleX = transform.localScale.x;
    }

    private void Update()
    {
        
        isGrounded = (Physics2D.Raycast((new Vector2(this.transform.position.x, this.transform.position.y + 1f)), Vector3.down, 5f, 1 << LayerMask.NameToLayer("Ground")));
        

        if (isGrounded)
            doubleJumped = false;
        Movement();
        CheckChargeTime();
        CheckChangeInKickDirection();
        CheckCheer();
    }

    private void CheckCheer ()
	{
		if (cheerPower <= 5) {
		}
		if (cheerPower >= 90) {
			GameInfoManager.Instance.AddPoints(this);
			GameInfoManager.Instance.cheerBarSet();
		}
	}


    private void Movement ()
	{
		float xMov = Input.GetAxis (input.Horizontal);

        if(rb.velocity.y != 0f)
        {
            shouldMoveX = false;
        }
        else
        {
            shouldMoveX = true;
        }

		if (xMov != 0 && !isChargingKick && canMove) {
			charAnim.SetBool ("isMoving", true);
			if ((xMov < 0 && isFacingRight) || (xMov > 0 && !isFacingRight)) {
				isFacingRight = !isFacingRight;
				currentScaleX = -currentScaleX;
				transform.localScale = new Vector3 (currentScaleX, transform.localScale.y); 
			}
            
            if(shouldMoveX)
			    rb.velocity = new Vector2 (movementSpeed * xMov, rb.velocity.y);
		} else {
			charAnim.SetBool ("isMoving", false);
            if (shouldMoveX)
            {
                rb.velocity = new Vector2(0f, rb.velocity.y);
            } 
			
		}
        bool isFacingWall = Physics2D.Raycast((new Vector2(this.transform.position.x, this.transform.position.y + 1f)), Vector3.right, 3f, 1 << LayerMask.NameToLayer("Ground")) || Physics2D.Raycast((new Vector2(this.transform.position.x, this.transform.position.y + 2f)), Vector3.right, 3f, 1 << LayerMask.NameToLayer("Ground")) || Physics2D.Raycast((new Vector2(this.transform.position.x, this.transform.position.y)), Vector3.right, 3f, 1 << LayerMask.NameToLayer("Ground"));

        if (isFacingWall)
        {
            Debug.Log("sssaaaasasas");
            rb.velocity = new Vector2(0f, rb.velocity.y);
        }

		if (rb.velocity.y < 0) {
				rb.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
			}
		if (Input.GetButtonDown (input.Fire1)) {

            if(isGrounded)
            {
                rb.AddForce(Vector2.up * jumpForce);
                
            }
            else if(!doubleJumped)
            {
                rb.velocity = new Vector2(movementSpeed * xMov, rb.velocity.y);
                rb.velocity = new Vector2(rb.velocity.x, 0f);
                rb.AddForce(Vector2.up * jumpForce);
                doubleJumped = true;
            }
            //Debug.Log("teste1");
            
        }

        if (Input.GetButtonDown(input.Fire2))
        {
            if(!isKicking)
            {
                Debug.Log("teste2");
                BeginKickCharge();
            }
            
        }

        if (Input.GetButtonUp(input.Fire2))
        {
            if(!isKicking && isChargingKick)
            {
                Debug.Log("teste3");
                charAnim.SetBool("kick", true);
                StartCoroutine( Kick(GetKickForce()));
            }
            
        }

    }

    private void BeginKickCharge()
    {
        charAnim.SetTrigger("preKick");
        currentChargeTime = 0f;
        currentKickDirection = 15f;
        isChargingKick = true;
		kickArrow.SetActive(true);
        canChangeKickDirection = true;
        if(isFacingRight)
        {
            kickArrow.transform.eulerAngles = new Vector3(0, 0, 15f);
        }
        else
        {
            kickArrow.transform.eulerAngles = new Vector3(0, 0, -15f);
        }
        
    }

    private void CheckChargeTime()
    {
        if(isChargingKick)
        {
            currentChargeTime += Time.deltaTime;
            if(currentChargeTime >= chargeTime)
            {
                charAnim.SetBool("kick", true);
                StartCoroutine(Kick(maxAttackPower));
            }
        }
    }

    private float GetKickForce()
    {
        return (minAttackPower + ((maxAttackPower - minAttackPower) * (currentChargeTime / chargeTime)));
    }

    private IEnumerator Kick(float force)
    {
        
        yield return new WaitForSeconds(0.2f);
        isKicking = true;
        isChargingKick = false;
        canChangeKickDirection = false;

        kickArrow.SetActive(false);
        kickBoxBehaviour.kickDirection = currentKickDirection;
        kickBoxBehaviour.kickForce = force;
        kickBoxBehaviour.applyKickToRight = isFacingRight;
        kickBoxCollider.enabled = true;

        yield return new WaitForSeconds(0.6f);

        kickBoxCollider.enabled = false;

        isKicking = false;
        charAnim.SetBool("kick", false);
    }

    private void CheckChangeInKickDirection()
    {
        if (!canChangeKickDirection)
            return;
        float axis = Input.GetAxis(input.Vertical);
        if (axis != 0)
        {
            currentKickDirection += (axis * rotationSpeedKickDirection * Time.deltaTime);
            currentKickDirection =  Mathf.Clamp(currentKickDirection, -15f, 45f);
            //kickArrow.transform.eulerAngles = (new Vector3(0, 0, currentKickDirection / 360));
            kickArrow.transform.Rotate(new Vector3(0,0,currentKickDirection/270f));
        }
    }

    public void ApplyDamage()
    {
        Debug.Log("sananasiudhbasd");
        StartCoroutine(ApplyHitEffects());
    }

    private IEnumerator ApplyHitEffects()
    {
        canMove = false;
        isInvencible = true;
        yield return new WaitForSeconds(1f);
        canMove = true;
        isInvencible = false;
    }

	private void OnCollisionEnter2D(Collision2D collision) {
 
    	if (collision.gameObject.tag == "Player") {
    		Physics2D.IgnoreCollision(collision.collider, GetComponent<Collider2D>());
        }
        if (collision.gameObject.tag == "Target")
        {
            Physics2D.IgnoreCollision(collision.collider, GetComponent<Collider2D>());
        }

    }

}
