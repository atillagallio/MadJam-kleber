﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attractor : MonoBehaviour {

    public Rigidbody2D rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        Attractive[] allAttractives = FindObjectsOfType<Attractive>();
        foreach(Attractive otherAttractive in allAttractives)
        {
            Attract(otherAttractive);
        }
    }

    void Attract(Attractive other)
    {
        Rigidbody2D otherRigidBody = other.rb;

        Vector2 direction = rb.position - otherRigidBody.position;
        float distance = direction.magnitude;

        //Debug.Log(distance.ToString());

        float forceMagnitude = (rb.mass * otherRigidBody.mass) / Mathf.Pow(distance, 2);
        Vector2 force = direction.normalized * forceMagnitude;

        //Debug.Log(force.ToString());

        otherRigidBody.AddForce(force);
    }
}
