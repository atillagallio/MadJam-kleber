﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player{

	public InputList input;
	public Joystick joystick;
	public bool ready;
	public CharacterClass character;

	public Player (InputList input, Joystick joystick, CharacterClass character)
	{
		this.input = input;
		this.joystick = joystick;
		this.character = character;
		this.character.characterPrefab.GetComponent<Character>().input = input;
		Debug.Log(this.input.Fire1);
		this.ready = false;

	}
}

public class InputList{
	public string Horizontal;
	public string Vertical;
	public string Jump;
	public string Fire1;
	public string Fire2;
	public string Fire3;
	public string Start;
}

public class Joystick{
	public string name;
	public int position;
}
