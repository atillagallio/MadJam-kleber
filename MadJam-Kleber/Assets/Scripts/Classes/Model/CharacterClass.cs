﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterClass{

	public string name;
	public string country;
	public Texture2D photo;
	public Texture2D hudName;
	public GameObject characterPrefab;
	public AudioClip audioClip;

	public CharacterClass (string _name, string _country, Texture2D _photo, Texture2D _hudName, GameObject _charPrefab, AudioClip _audio)
	{
		this.name = _name;
		this.country = _country;
		this.photo = _photo;
		this.hudName = _hudName;
		this.characterPrefab = _charPrefab;
		this.audioClip = _audio;

	}
}
