﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class GameInfoManager : Singleton<GameInfoManager> {


    public BallBehaviour ball;
    public float test = 45f;

    public Transform[] spawnPoints;
    public List<GameObject> charObjs;

    public GameObject target;
    public Transform[] targetSpawnPoints;
    public bool hasTarget = false;

    public float cheerBar = 100f;
    public float time = 0f;
    public bool canUpdate = true;

   	public bool endGame = false;

    public Camera mainCamera;

    public Player winnerPlayer;

	public void RestartGame ()
	{
		MainMenuManager.Instance.RestartInfo();
		SceneManager.LoadScene(0);
	}
    private void Start ()
	{
		time = 0f;
		charObjs = new List<GameObject> ();
		PlayerListManager.Instance.InstantiateGamePlayers (charObjs);
		MultipleTargetCamera cameraScript = mainCamera.GetComponent<MultipleTargetCamera>();
		foreach (GameObject obj in charObjs) {
			cameraScript.AddTarget(obj.transform);
		}
		cheerBarSet();

	}

	public void AddPoints (Character thisChar)
	{
		thisChar.points++;
		thisChar.gameHUD.transform.GetChild(1).GetComponent<TextMeshProUGUI>().text = thisChar.points.ToString();
		CheckIfWon(thisChar);
	}

	public void CheckIfWon (Character thisChar)
	{
		if (thisChar.points >= 3) {
			EndGame(thisChar);
		}
	}

	void Update ()
	{
		if (canUpdate) {
			CheckTime();
		}
	}

	public void CheckTime ()
	{
		time += Time.deltaTime;
		if (time >= 60 * 5) {
		canUpdate = false;
		EndGame(CheckWhoWonByTime());
		}

        if (((int)time) % 30 == 0)
        {
            CheckToSpawnTarget();
        }

	}

    private void CheckToSpawnTarget()
    {
        if(!hasTarget)
        {
            int rng = Random.Range(0, targetSpawnPoints.Length);
            Instantiate(target, targetSpawnPoints[rng].transform.position, Quaternion.identity);
            hasTarget = true;
        }
    }

	public Character CheckWhoWonByTime ()
	{
		List<Character> charStanding = new List<Character> ();
		Character winner = new Character();
		int i = 0;
		foreach (GameObject gobj in charObjs) {
			Character _char = gobj.GetComponent<Character> ();
			if (i == 0) {
				winner = _char;
			}
			if (winner.points == _char.points) {
				if (winner.cheerPower < _char.cheerPower) {
					winner = _char;
				}
			} else if(winner.points < _char.points) {
				_char = winner;
			}
			i++;
		}
		return winner;
	}
	public void EndGame (Character winner)
	{
		winnerPlayer = winner.player;
		endGame = true;
		HudManager.Instance.StartSettingEndHUD();
	}

	public void cheerBarSet ()
	{
		float eachValue = cheerBar / charObjs.Count;
		foreach (GameObject obj in charObjs) {
			obj.GetComponent<Character>().cheerPower = eachValue;
		}
		HudManager.Instance.SetHudUp();
	}

	public float GetPowerDiff (float attacking, float defending)
	{
		if (attacking == 0) {
			attacking = 0.1f;
		}
		if (defending == 0) {
			defending = 0.1f;
		}
		if ((1f * attacking / defending) >= 5)
		{
			return 5f;
		}

		return 1f * attacking/defending; 
	}

	public void RedistributeBarFromPlayer (GameObject obj, float value, List<GameObject> listOfWinnerPlayers)
	{
		Character characterHit = obj.GetComponent<Character> ();
		float redistribuitedValue = characterHit.cheerPower;

		if ((characterHit.cheerPower - value) > 0) {
			redistribuitedValue = value;
			characterHit.cheerPower = characterHit.cheerPower - value;
		} else {
			characterHit.cheerPower = 0;
		}

		float valueToEach = value / listOfWinnerPlayers.Count;

		foreach (GameObject objFromList in listOfWinnerPlayers) {
			if (objFromList != obj) {
				objFromList.GetComponent<Character>().cheerPower += valueToEach;
			}
		} 

		HudManager.Instance.AttHud();
	}

    

}
