﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuManager : Singleton<MainMenuManager> {

	private List<bool> playerOn;
	private List<bool> canChangeList;
	private bool gameStarted = false;

	public List<GameObject> selectHud;
	public List<TextMeshProUGUI> P;

	public AudioSource menuAudio;
	public AudioClip whistle;

	// Use this for initialization
	void Start () {
		canChangeList = new List<bool>();
		canChangeList.Add(true);
		canChangeList.Add(true);
		canChangeList.Add(true);
		canChangeList.Add(true);
		playerOn = new List<bool>();
		playerOn.Add(false);
		playerOn.Add(false);
		playerOn.Add(false);
		playerOn.Add(false);
	}

	public void RestartInfo(){
		canChangeList = new List<bool>();
		canChangeList.Add(true);
		canChangeList.Add(true);
		canChangeList.Add(true);
		canChangeList.Add(true);
		playerOn = new List<bool>();
		playerOn.Add(false);
		playerOn.Add(false);
		playerOn.Add(false);
		playerOn.Add(false);

		gameStarted = false;
	}


	// Update is called once per frame
	void FixedUpdate ()
	{
		if (!gameStarted) {
			CheckPlayerInput(0);
			CheckPlayerInput(1);
			CheckPlayerInput(2);
			CheckPlayerInput(3);
			CheckStartGame();
		}
	}

	private IEnumerator ChangeCharacterCD (int index){
		yield return new WaitForSeconds(0.3f);
		canChangeList[index] = true;
	}

	void CheckStartGame ()
	{
		if (PlayerListManager.Instance.IsEveryoneReady()) {
			SceneManager.LoadScene(1);
			menuAudio.PlayOneShot(whistle);
			gameStarted = true;
		}

	}
	void PlayersNames (int index)
	{
		selectHud[index].SetActive(true);
		if (PlayerListManager.Instance.connectedPlayers.Count >= index+1) {
			ChangeHudImages(PlayerListManager.Instance.connectedPlayers [index].character, selectHud[index]);
			P[index].text = PlayerListManager.Instance.connectedPlayers [index].joystick.name;
			selectHud[index].GetComponent<RawImage>().enabled = true;
			selectHud[index].transform.GetChild(0).gameObject.SetActive(true);
			selectHud[index].transform.GetChild(1).gameObject.SetActive(true);
			selectHud[index].transform.GetChild(2).gameObject.SetActive(true);
			selectHud[index].transform.GetChild(3).gameObject.SetActive(true);
			selectHud[index].transform.GetChild(4).gameObject.SetActive(true);
			selectHud[index].transform.GetChild(5).gameObject.SetActive(false);
			if (PlayerListManager.Instance.connectedPlayers [index].ready == true) {
				P[index].color = Color.green;
				selectHud[index].transform.GetChild(1).gameObject.SetActive(false);
				selectHud[index].transform.GetChild(3).gameObject.SetActive(false);
				selectHud[index].transform.GetChild(4).gameObject.SetActive(false);
				selectHud[index].transform.GetChild(2).gameObject.GetComponent<RawImage>().color = Color.green;
			} else {
				P[index].color = Color.white;
				selectHud[index].transform.GetChild(1).gameObject.SetActive(true);
				selectHud[index].transform.GetChild(3).gameObject.SetActive(true);
				selectHud[index].transform.GetChild(4).gameObject.SetActive(true);
				selectHud[index].transform.GetChild(2).gameObject.GetComponent<RawImage>().color = Color.white;
			}
		} else {
			P[index].text = "";
			selectHud[index].GetComponent<RawImage>().enabled = false;
			selectHud[index].transform.GetChild(0).gameObject.SetActive(false);
			selectHud[index].transform.GetChild(1).gameObject.SetActive(false);
			selectHud[index].transform.GetChild(2).gameObject.SetActive(false);
			selectHud[index].transform.GetChild(3).gameObject.SetActive(false);
			selectHud[index].transform.GetChild(4).gameObject.SetActive(false);
			selectHud[index].transform.GetChild(5).gameObject.SetActive(true);
		}
	}

	void ChangeHudImages (CharacterClass _char, GameObject hud)
	{
		hud.transform.GetChild(0).gameObject.GetComponent<RawImage>().texture = _char.photo;
		hud.transform.GetChild(0).gameObject.GetComponent<RawImage>().SetNativeSize();
		hud.transform.GetChild(2).gameObject.GetComponent<RawImage>().texture = _char.hudName;
		hud.transform.GetChild(2).gameObject.GetComponent<RawImage>().SetNativeSize();
	}

	void CheckPlayerInput (int index)
	{
		if (Input.GetJoystickNames ().Length >= index+1) {
			Joystick joystick = new Joystick ();
			joystick.name = Input.GetJoystickNames () [index];
			joystick.position = index;
			string fire2 = "Fire2_P" + (index+1).ToString();
			string fire1 = "Fire1_P" + (index+1).ToString();
			string vertical = "Vertical_P" + (index+1).ToString();
			string start = "Start_P" + (index+1).ToString();

			if (playerOn[index]) { 
				if (Input.GetButtonDown (fire2)) {
					//GOBACK
					if (PlayerListManager.Instance.FindPlayer (joystick).ready) {
						PlayerListManager.Instance.FindPlayer (joystick).ready = false;
					} else {
						playerOn[index] = false;
						if (PlayerListManager.Instance.RemovePlayer (joystick)) {
							P[index].text = "";
						}
					}
				}
				if (Input.GetButtonDown (fire1)) {
					//CONFIRM
					PlayerListManager.Instance.FindPlayer (joystick).ready = true;
					menuAudio.PlayOneShot(PlayerListManager.Instance.FindPlayer(joystick).character.audioClip);
				}
				if (Input.GetAxis (vertical) != 0 && canChangeList [index]) {
					//Change Character
					Player player = PlayerListManager.Instance.FindPlayer (joystick);
					if (!player.ready) {
						canChangeList[index] = false;
						CharacterClass _char = PlayerListManager.Instance.ChangeCharacter (player, Input.GetAxis (vertical));
						StartCoroutine (ChangeCharacterCD (index));
					}	
				}
			} else {
				if (Input.GetButtonDown (start)) {
					//CONNECT
					playerOn[index] = true;
					PlayerListManager.Instance.AddPlayer (joystick);
				}
			}
			PlayersNames (index);
		}

	}
}