﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerListManager : Singleton<PlayerListManager> {

	public List<Player> connectedPlayers;
	private List<CharacterClass> characterList;
	public List<Texture2D> hudFaces;
	public List<Texture2D> hudNames;
	public List<GameObject> characterObj;
	public List<AudioClip> audioList;

	public void Awake ()
	{
		DontDestroyOnLoad(this.gameObject);
	}
	public void Start ()
	{
		characterList = new List<CharacterClass>();
		characterList.Add(new CharacterClass("Neymar", "Brazil", hudFaces[0], hudNames[0], characterObj[0], audioList[0]));
		characterList.Add(new CharacterClass("Tsubasa", "Japan", hudFaces[1], hudNames[1], characterObj[1], audioList[1]));
		characterList.Add(new CharacterClass("Messi", "Argentina", hudFaces[2], hudNames[2], characterObj[2], audioList[2]));
		characterList.Add(new CharacterClass("C.R.7", "Portugal", hudFaces[3], hudNames[3], characterObj[3], audioList[3]));	
		connectedPlayers = new List<Player>();

	}

	public Player FindPlayerByCharacter (Character _char)
	{
		foreach (Player player in connectedPlayers) {
			if (player.character.characterPrefab.GetComponent<Character> () == _char) {
				return player;
			}
		}
		return null;
	}

	public void InstantiateGamePlayers (List<GameObject> objList)
	{
        int _index = 0;
		foreach (Player player in connectedPlayers) {

			GameObject prefab = Instantiate(player.character.characterPrefab, GameInfoManager.Instance.spawnPoints[_index].position, Quaternion.identity);
			prefab.GetComponent<Character>().input = player.input;
			prefab.GetComponent<Character>().player = player;
			objList.Add(prefab);
			_index++;
		}
	}

	public Player FindPlayer (Joystick joystick)
	{
		foreach (Player player in connectedPlayers) {
			if (((player.joystick.name == joystick.name) && (player.joystick.position == joystick.position)) || (connectedPlayers.Count >= 4)) {
				return player;
			}  
		}
		return null;
	}

	private int FindCharacterIndex (CharacterClass _character)
	{
		int i = 0;
		foreach (CharacterClass character in characterList) {
			if (character.name == _character.name) {
				return i;
			}
			i++;
		}
		return 0;
	}

	public CharacterClass ChangeCharacter (Player Player, float i)
	{	
		int index = 0;
		index = FindCharacterIndex(Player.character);

		if (i > 0) {
			if (index == characterList.Count - 1) {
				Player.character = characterList [0];
			} else {
				Player.character = characterList[index + 1];
			}

		} else {
			if (index == 0) {
				Player.character = characterList [characterList.Count - 1];
			} else {
				Player.character = characterList[index - 1];
			}
		}
		return Player.character;
	}

	public bool IsEveryoneReady ()
	{
		bool ready = true;
		foreach (Player player in connectedPlayers) {
			ready = ready && player.ready;
		}
		if (connectedPlayers.Count >= 2) {
			return ready;
		} else {
			return false;
		}
	}

	public bool AddPlayer (Joystick joystick)
	{
		foreach (Player player in connectedPlayers) {
			if (((player.joystick.name == joystick.name) && (player.joystick.position == joystick.position)) || (connectedPlayers.Count >= 4)) {
				return false;
			}  
		}
		InputList input = InstantiateInputs(joystick);
		connectedPlayers.Add(new Player(input, joystick, characterList[0]));
		return true;
	}

	private InputList InstantiateInputs(Joystick joystick){
		InputList input = new InputList();
		input.Fire1 = "Fire1_P" + (joystick.position+1).ToString();
		input.Fire2 = "Fire2_P" + (joystick.position+1).ToString();
		input.Fire3 = "Fire3_P" + (joystick.position+1).ToString();
		input.Jump = "Jump_P" + (joystick.position+1).ToString();
		input.Start = "Start_P" + (joystick.position+1).ToString();
		input.Horizontal = "Horizontal_P" + (joystick.position+1).ToString();
		input.Vertical = "Vertical_P" + (joystick.position+1).ToString();

		return input;

	}

	public bool RemovePlayer (Joystick joystick)
	{
		foreach (Player player in connectedPlayers) {
			if (((player.joystick.name == joystick.name) && (player.joystick.position == joystick.position))) {
				connectedPlayers.Remove(player);
				return true;
			}  
		}
		return false;
	}
}
