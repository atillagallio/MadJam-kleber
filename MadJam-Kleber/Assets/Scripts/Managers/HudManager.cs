﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class HudManager : Singleton<HudManager> {

	public GameObject cheerBar;
	public List<GameObject> eachBar;
	public TextMeshProUGUI timer;
	public List<GameObject> eachHUD;

	public float onePerc = 8.8f;

    [Header("End Game Panel Info")]
    public TextMeshProUGUI endGameText;
    public GameObject endGamePanel;
    public TextMeshProUGUI winnerP_indexText;
    public RawImage winnerSprite;
    public TextMeshProUGUI winnerPointsText;
    public Button playAgain;

	// Use this for initialization
	void Start ()
	{
		
	}

	public void SetHudUp(){
		int i = 0;
		float curX = 0f - (onePerc * 50) + 5;

		foreach (GameObject charobj in GameInfoManager.Instance.charObjs) {
			//Debug.Log(i);
			Character character = charobj.GetComponent<Character>();
			RectTransform rt = eachBar[i].GetComponent<RectTransform>();
			float xSize = character.cheerPower*onePerc;
			rt.sizeDelta = new Vector2(xSize, 21f);

			rt.localPosition = new Vector3(curX, rt.localPosition.y);
			eachBar [i].SetActive (true);
			eachHUD [i].transform.GetChild(0).GetComponent<RawImage>().texture = charobj.GetComponent<Character>().player.character.photo;
			eachHUD [i].transform.GetChild(0).GetComponent<RawImage>().SetNativeSize();
			eachHUD[i].SetActive (true);
			charobj.GetComponent<Character>().gameHUD = eachHUD[i];
			curX += xSize;
			i++;
		}
		int charCount = GameInfoManager.Instance.charObjs.Count;
		int playersDif = eachBar.Count - charCount;
		while (playersDif > 0) {
			eachBar.RemoveAt(eachBar.Count - 1);
			playersDif = eachBar.Count - charCount;
		}

	}

	public void AttHud ()
	{
		int i = 0;
		float curX = 0f - (onePerc * 50) + 5;

		foreach (GameObject charobj in GameInfoManager.Instance.charObjs) {
			Debug.Log(i);
			Character character = charobj.GetComponent<Character>();
			RectTransform rt = eachBar[i].GetComponent<RectTransform>();
			float xSize = character.cheerPower*onePerc;
			rt.sizeDelta = new Vector2(xSize, 21f);

			rt.localPosition = new Vector3(curX, rt.localPosition.y);
			eachBar [i].SetActive (true);
			curX += xSize;
			i++;
		}
	}

	// Update is called once per frame
	void Update ()
	{
		if (GameInfoManager.Instance.endGame) {
			if(Input.GetButtonDown("Submit")){
				//GameInfoManager.Instance.RestartGame();
			}
		} else {
			timer.text = Mathf.Round (GameInfoManager.Instance.time).ToString ();
		}
	}

    public void StartSettingEndHUD()
    {
        StartCoroutine(SetEndHUD(GameInfoManager.Instance.winnerPlayer));
    }


    public IEnumerator SetEndHUD(Player winnerPlayer)
    {
        endGameText.gameObject.SetActive(true);
        winnerP_indexText.text = "Player " + (winnerPlayer.joystick.position + 1).ToString() + " Won!!";
        winnerSprite.texture = winnerPlayer.character.photo;
        endGamePanel.SetActive(true);
		winnerPointsText.text = winnerPlayer.character.characterPrefab.GetComponent<Character>().points.ToString();
        yield return new WaitForSeconds(1.5f);

    }
}
