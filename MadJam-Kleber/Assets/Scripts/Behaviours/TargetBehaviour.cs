﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetBehaviour : MonoBehaviour {

    public bool faceLeft;

    private void Start()
    {
        if(!faceLeft)
        {
            this.transform.localScale = new Vector3(-2f, 2f);
        }
    }
}
