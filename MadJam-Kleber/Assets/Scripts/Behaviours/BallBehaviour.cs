﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallBehaviour : MonoBehaviour {

    public bool isInKickMode;
    public Rigidbody2D rb;
    public GameObject charKicking;
    private float force;
    private Transform respaw;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        respaw = rb.gameObject.transform;
    }

    private void Update ()
	{
		//Debug.Log(rb.velocity.magnitude.ToString());
		isInKickMode = rb.velocity.magnitude > 12.5f;
		if (rb.gameObject.transform.position.y <= -5f) {
			rb.transform.position = respaw.position;
		} 

        if(rb.velocity.magnitude <= 8f)
        {
            charKicking = null;
        }
    }

    

    private void OnCollisionEnter2D (Collision2D collision)
	{
		if (isInKickMode && collision.transform.tag == "Player") {
			//force = rb.velocity.magnitude;
           
			Character charToHit = collision.transform.GetComponent<Character> ();
			float points = Mathf.Abs (Mathf.Round ((rb.velocity.SqrMagnitude () / 8)));
			if (charToHit && !charToHit.isInDash && !charToHit.isInvencible && charKicking != null) {
                //Vector2 dir = collision.contacts[0].point - new Vector2(this.transform.position.x,this.transform.position.y);
                //dir = -dir.normalized;
                //Debug.Log("saas " + dir.ToString());
                charToHit.charAnim.SetTrigger("getHit");
				float str = GameInfoManager.Instance.GetPowerDiff (charKicking.GetComponent<Character> ().cheerPower, charToHit.cheerPower);
				if (rb.velocity.x > 0f) {
					
					List<GameObject> objKickingList = new List<GameObject> ();
					objKickingList.Add (charKicking);
					if (charKicking == collision.transform.gameObject) {
						
						//GameInfoManager.Instance.RedistributeBarFromPlayer(collision.transform.gameObject, points, GameInfoManager.Instance.charObjs);
					} else {
						GameInfoManager.Instance.AddPoints(charKicking.GetComponent<Character>());
						GameInfoManager.Instance.RedistributeBarFromPlayer(collision.transform.gameObject, points, objKickingList);
					}

                    charToHit.transform.GetComponent<Rigidbody2D>().AddForce(new Vector2(1f,1f) * str * 300f);
					Debug.Log(points);
                }
                else
                {
					List<GameObject> objKickingList = new List<GameObject>();
					objKickingList.Add(charKicking);
					GameInfoManager.Instance.RedistributeBarFromPlayer(collision.transform.gameObject, points, objKickingList);
					charToHit.transform.GetComponent<Rigidbody2D>().AddForce(new Vector2(-1f, 1f) *str * 300f);
					Debug.Log(points);
	
                }
                
                
                charToHit.ApplyDamage();
            }
                
        }

        if(collision.transform.tag == "Target" && charKicking != null)
        {
			GameInfoManager.Instance.AddPoints (charKicking.GetComponent<Character>());
            GameInfoManager.Instance.hasTarget = false;
            Destroy(collision.gameObject);
        }

    }
}
