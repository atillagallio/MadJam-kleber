﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fade : MonoBehaviour {

	public float minimum = 0.0f;
    public float maximum = 1f;
     public float duration = 1.0f;
	private RawImage image;
	private float startTime;
	// Use this for initialization
	void Start () {
		image = GetComponent<RawImage>();
		startTime = Time.time;
		
	}
	
	// Update is called once per frame
	void Update () {
	float t = (Time.time - startTime) / duration;
		image.color = new Color(1f,1f,1f,Mathf.SmoothStep(1f, 0, t));		
	}

}
