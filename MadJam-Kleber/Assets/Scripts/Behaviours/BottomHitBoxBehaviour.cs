﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BottomHitBoxBehaviour : MonoBehaviour {

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.transform.tag == "Head")
        {
            Character charToHit = collision.transform.GetComponent<Character>();
            Character thisChar = GetComponentInParent<Character>();
            if(charToHit)
            {
                GameInfoManager.Instance.AddPoints(thisChar);
                Debug.Log("head damage");
                thisChar.transform.GetComponent<Rigidbody2D>().AddForce(Vector2.up * 150f);
            }
        }
    }
}
