﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KickHitBoxBehaviour : MonoBehaviour {

    [HideInInspector]
    public float kickForce;
    [HideInInspector]
    public float kickDirection;
    [HideInInspector]
    public bool applyKickToRight;

    /*
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.transform.tag == "Ball" && canApplyKick)
        {
            canApplyKick = false;
            Debug.Log("kicked");
            BallBehaviour ball = collision.transform.GetComponent<BallBehaviour>();
            if(ball != null)
            {
                Debug.Log("o carai");
                //Vector2 kickDir = new Vector2(Mathf.Abs( Mathf.Sin(kickDirection)), Mathf.Abs(Mathf.Cos(kickDirection))) ;
                Vector2 kickDir = new Vector2(Mathf.Sin(kickDirection), Mathf.Cos(kickDirection));
                Debug.Log(kickForce.ToString());
                Debug.Log(kickDir.ToString());
                ball.rb.AddForce(kickDir * kickForce * 100f);
                //ball.rb.AddForce(Vector2.up * 2000f);
            }
            
        }
    }
    */

    private void OnTriggerEnter2D(Collider2D collision)
    {
        Debug.Log("asd");
        if (collision.transform.tag == "Ball")
        {
            
            //Debug.Log("kicked");
            BallBehaviour ball = collision.transform.GetComponent<BallBehaviour>();
            ball.charKicking = this.gameObject.transform.parent.gameObject;
            if (ball != null)
            {
                //Debug.Log("o carai");
                //Vector2 kickDir = new Vector2(Mathf.Abs( Mathf.Sin(kickDirection)), Mathf.Abs(Mathf.Cos(kickDirection))) ;
                //Vector2 kickDir = new Vector2(Mathf.Sin(kickDirection), Mathf.Cos(kickDirection));
                float xComp;
                xComp = applyKickToRight ? (90f - Mathf.Abs(kickDirection)) / 90f : -((90f - Mathf.Abs(kickDirection)) / 90f);
                xComp = Mathf.Clamp(xComp, -1f, 1f);
                float yComp = kickDirection / 90f;
                yComp = Mathf.Clamp(yComp, -1f, 1f);
                Vector2 jooj = new Vector2(xComp, yComp);
                //Debug.Log(kickForce.ToString());
                //Debug.Log(kickDir.ToString());
                ball.rb.AddForce(jooj * kickForce * 100f);
                //ball.rb.AddForce(Vector2.up * 2000f);
            }

        }
    }

    /*
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.transform.tag == "Ball" && canApplyKick)
        {
            canApplyKick = false;
            Debug.Log("kicked");
            BallBehaviour ball = collision.transform.GetComponent<BallBehaviour>();
            if (ball != null)
            {
                Debug.Log("o carai");
                //Vector2 kickDir = new Vector2(Mathf.Abs( Mathf.Sin(kickDirection)), Mathf.Abs(Mathf.Cos(kickDirection))) ;
                Vector2 kickDir = new Vector2(Mathf.Sin(kickDirection), Mathf.Cos(kickDirection));
                Debug.Log(kickForce.ToString());
                Debug.Log(kickDir.ToString());
                ball.rb.AddForce(kickDir * kickForce * 100f);
                //ball.rb.AddForce(Vector2.up * 2000f);
            }

        }
    }
    */

}
