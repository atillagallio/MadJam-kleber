﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportPlatformBehaviour : MonoBehaviour {

    public bool isLeft;
    public bool isDown;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (isLeft)
        {
            //Debug.Log("1");
            //collision.transform.Translate(new Vector3(50f, collision.transform.position.y));
            collision.transform.position = new Vector3(50f, collision.transform.position.y);
        }
        else if(isDown)
        {
            //Debug.Log("2");
            //collision.transform.Translate(new Vector3(collision.transform.position.x, 44f));
            collision.transform.position = new Vector3(collision.transform.position.x, 44f);
        }
        else
        {
            //Debug.Log("3");
            //collision.transform.Translate(new Vector3(-50f, collision.transform.position.y));
            collision.transform.position = new Vector3(-50f, collision.transform.position.y);
        }
    }
}
